import { InputLabel, MenuItem, Select, TextField } from "@material-ui/core";
import axios from "axios";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import styles from "../styles/Home.module.css";
const Form = () => {
  const [showMessage, setShowMessage] = useState(false);
  const [message, setMessage] = useState();
  const { control, handleSubmit, watch, reset } = useForm();
  const url =
    process.env.NODE_ENV === "development"
      ? "http://localhost:3000"
      : "https://rsvp.jaycobjones.com";
  const onSubmit = async (data) => {
    await axios
      .post(`${url}/api/rsvp`, {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        attending: data.attending,
        numberOfGuest: data.numberOfGuest,
        comments: data.comments,
      })
      .then((res) => {
        console.log(res);
        setShowMessage(true);
        setMessage(res.data.message);
      });
    reset();
  };
  return (
    <>
      {showMessage ? (
        <p>
          {message} You can return to the home page by clicking{" "}
          <a href="/">here</a>.
        </p>
      ) : (
        <form onSubmit={handleSubmit(onSubmit)}>
          {console.log(url)}
          <div>
            <Controller
              name="firstName"
              control={control}
              defaultValue=""
              rules={{ required: true }}
              render={({ field }) => (
                <TextField
                  className={styles.inputFix}
                  required
                  label="First Name"
                  {...field}
                />
              )}
            />
            <Controller
              name="lastName"
              control={control}
              defaultValue=""
              rules={{ required: true }}
              render={({ field }) => (
                <TextField required label="Last Name" {...field} />
              )}
            />
          </div>
          <div>
            <Controller
              name="email"
              control={control}
              defaultValue=""
              rules={{ required: true }}
              render={({ field }) => (
                <TextField required label="Email" {...field} />
              )}
            />
          </div>
          <div style={{ padding: "25px" }}>
            <Controller
              name="attending"
              control={control}
              defaultValue=""
              rules={{ required: true }}
              render={({ field }) => (
                <>
                  <InputLabel>Will you be attending?</InputLabel>
                  <Select {...field}>
                    <MenuItem value="yes">Yes, I'll be there</MenuItem>
                    <MenuItem value="no">Sorry, can't make it</MenuItem>
                    <MenuItem value="maybe">Maybe</MenuItem>
                  </Select>
                </>
              )}
            />
          </div>
          {watch("attending") !== "no" ? (
            <>
              <>
                {watch("attending") === "yes" && (
                  <p className="message-alt">That is great news!</p>
                )}
                {watch("attending") === "maybe" && (
                  <p className="message-alt">Well, we hope you can make it!</p>
                )}
              </>
              <div style={{ padding: "25px" }}>
                <Controller
                  name="numberOfGuest"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <>
                      <InputLabel>How many will be joining?</InputLabel>
                      <Select value="1" {...field}>
                        <MenuItem value="1">1</MenuItem>
                        <MenuItem value="2">2</MenuItem>
                        <MenuItem value="3">3</MenuItem>
                        <MenuItem value="4">4</MenuItem>
                        <MenuItem value="5">5</MenuItem>
                        <MenuItem value="6">6</MenuItem>
                      </Select>
                    </>
                  )}
                />
              </div>
            </>
          ) : (
            <p className="message-alt">
              That is okay! It is a busy time of the year.
            </p>
          )}

          <div>
            <Controller
              name="comments"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField multiline={true} label="Comments" {...field} />
              )}
            />
          </div>
          <input type="submit" />
        </form>
      )}
    </>
  );
};

export default Form;
