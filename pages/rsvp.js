import Form from "../components/Form";
import styles from "../styles/Home.module.css";
const RSVP = () => {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>RSVP</h1>
        <p className={styles.description}>
          Please fill out the form below if you can or cannot make it. We hope
          to see you July 10th.
        </p>
        <div>
          <Form />
        </div>
      </main>
    </div>
  );
};

export default RSVP;
