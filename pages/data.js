import axios from "axios";
import { useEffect, useState, setRef } from "react";

const Data = () => {
  const [allData, setData] = useState();
  const [passcode, setPasscode] = useState();
  let count = 0;
  const url =
    process.env.NODE_ENV === "development"
      ? "http://localhost:3000"
      : "https://rsvp.jaycobjones.com";
  useEffect(() => {
    const getData = async () => {
      await axios
        .get(`${url}/api/rsvp`)
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => console.log(err));
    };
    getData();
  }, []);

  return (
    <div>
      {console.log(url)}
      {passcode === "2021" ? (
        <>
          {allData && allData.length + " Families have RSVP"}
          {allData?.map((x) => {
            if (x.attending === "yes" || x.attending === "maybe") {
              count = parseInt(x.numberOfGuest, 10) + count;
            }

            return (
              <div style={{ margin: "30px" }}>
                <div>
                  <strong>ID:</strong> {x.id}
                </div>
                <div>
                  <strong>First Name:</strong> {x.firstName}
                </div>
                <div>
                  <strong>Last Name:</strong> {x.lastName}
                </div>
                <div>
                  <strong>Email:</strong> {x.email}
                </div>
                <div>
                  <strong>Attending:</strong> {x.attending}
                </div>
                <div>
                  <strong>Guest Count:</strong> {x.numberOfGuest}
                </div>
                <div>
                  <strong>Comment:</strong> {x.comments}
                </div>
              </div>
            );
          })}
          Number of people: {count}
        </>
      ) : (
        <label>
          Passcode{" "}
          <input
            type="text"
            value={passcode}
            onChange={(e) => setPasscode(e.target.value)}
          />
        </label>
      )}
    </div>
  );
};

export default Data;
