import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <>
      <div
        className="headlineParent"
        style={{
          backgroundImage: `url("${require("../public/header.jpg")}")`,

          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPositionX: "50%",
          display: "flex",
          justifyContent: "center",
          padding: "30px 0",
        }}
      >
        <div className="headlineHolder">
          <div>
            <Image
              src="/boygirl.png"
              alt="Gender Reveal"
              width={250}
              height={84}
            />
          </div>
          <div>
            <Image
              src="/genderReveal.png"
              alt="Gender Reveal"
              width={250}
              height={57}
            />
          </div>
          <div style={{ marginTop: "30px" }}>
            <Image
              src="/jk.png"
              alt="Jaycob and Kelsey"
              width={250}
              height={115}
            />
          </div>
        </div>
      </div>
      <div className={styles.container}>
        <Head>
          <title>Gender Reveal RSVP</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <p className={styles.description} style={{ marginTop: "20px" }}>
            We would like you to join us to find out what our baby will be.
          </p>

          <div className={styles.grid}>
            <Link href="/rsvp">
              <a className={styles.card}>
                <h2>RSVP &rarr;</h2>
                <p>
                  If you can or can't make it, please click here to RSVP
                  <br />
                </p>
                <br />
                <small>click to RSVP</small>
              </a>
            </Link>
            <a
              target="_blank"
              href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=MW5lMGV2aDkzbjE4bmNxZm9la2c5N3ZsYXIgam9uZXNqYXljb2JAbQ&amp;tmsrc=jonesjaycob%40gmail.com"
              className={styles.card}
            >
              <h2>Date and Time &rarr;</h2>
              <p>
                July 10, 2021 <br /> At 5:00 pm
              </p>
              <br />
              <small>click to add to your calender</small>
            </a>
            <a
              href="https://goo.gl/maps/gQmhUhC1zCePcs8X9"
              target="_blank"
              className={styles.card}
            >
              <h2>Location &rarr;</h2>
              <p>
                4847 Jacmel Ct
                <br /> Sparks, NV 89436
              </p>
              <br />
              <small>click for directions</small>
            </a>

            <a className={styles.card}>
              <h2>Food and Drinks </h2>
              <p>
                Come hungry and thirsty because food and drinks will be
                available.
              </p>
              <br />
              <small>&nbsp;</small>
            </a>
            <a className={styles.card}>
              <h2>Seating</h2>
              <p>
                We will have many seats but we also encourage everyone to bring
                a lawn chair.
              </p>
              <br />
              <small>&nbsp;</small>
            </a>
          </div>
        </main>
      </div>
    </>
  );
}
