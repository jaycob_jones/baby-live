-- AlterTable
ALTER TABLE "User" ADD COLUMN     "emailValidated" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "Baby" (
    "id" SERIAL NOT NULL,
    "firstName" VARCHAR(255),
    "lastName" VARCHAR(255),
    "email" VARCHAR(255) NOT NULL,
    "attending" VARCHAR(255),
    "numberOfGuest" VARCHAR(255),
    "comments" VARCHAR(255),

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Baby.email_unique" ON "Baby"("email");
